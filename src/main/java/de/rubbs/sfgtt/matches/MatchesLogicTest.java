package de.rubbs.sfgtt.matches;

import de.rubbs.sfgtt.db.Match;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class MatchesLogicTest {


    @Test
    public void testParseCsv() throws IOException {
        MatchesLogic matchesLogic = new MatchesLogic(null);
        FileReader file = new FileReader("/home/ruben/projekte/tt-homepage/src/test/resources/Vereinsspielplan.csv");
        BufferedReader reader = new BufferedReader(file);
        List<Match> matches = matchesLogic.parseCsv(reader);

        assertNotNull(matches);
        assertFalse(matches.isEmpty());
    }
}