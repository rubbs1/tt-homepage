package de.rubbs.sfgtt.matches;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import de.rubbs.sfgtt.db.Match;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Handle file uploads
 * Created by ruben on 28.12.15.
 */
@Slf4j
public class MatchesServlet extends HttpServlet {

    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    private Random rand = new Random();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        MatchesLogic matchesLogic = new MatchesLogic(blobstoreService);

        List<Match> matches = matchesLogic.handleGet();

        ObjectMapper om = new ObjectMapper();
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(om.writeValueAsString(matches));
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("received new data...");

        Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
        List<BlobKey> blobKeys = blobs.get("myFile");

        if (blobKeys == null || blobKeys.isEmpty()) {
            log.error("no file to upload");
            resp.sendRedirect("/");
        } else {
            log.info("parse received data");
            MatchesLogic matchesLogic = new MatchesLogic(blobstoreService);
            matchesLogic.handlePost(blobKeys.get(0).getKeyString());
        }

        resp.sendRedirect("/#/spielplan");

    }
}
