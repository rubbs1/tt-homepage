package de.rubbs.sfgtt.matches;

import au.com.bytecode.opencsv.CSVReader;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.blobstore.BlobstoreService;
import de.rubbs.sfgtt.db.Match;
import de.rubbs.sfgtt.db.OfyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
class MatchesLogic {

    private final BlobstoreService blobstoreService;

    List<Match> handleGet() {

        //return list of matches
        return OfyService.ofy().load().type(Match.class).list();
    }

    void handlePost(String blobstoreKey) throws IOException {
        BlobKey blobKey = new BlobKey(blobstoreKey);

        BlobstoreInputStream is = new BlobstoreInputStream(blobKey);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "ISO-8859-15"));

        deleteExistingMatches();

        List<Match> matches = parseCsv(bufferedReader);
        log.info("parsed " + matches.size() + " new matches");

        //write match to datastore
        OfyService.ofy().save().entities(matches).now();
    }

    List<Match> parseCsv(BufferedReader bufferedReader) throws IOException {
        List<Match> parsedMatches = new ArrayList<>();

        CSVReader csvReader = new CSVReader(bufferedReader, ';');

        // read headline
        if (null == csvReader.readNext()) {
            log.error("empty csv file");
            return parsedMatches;
        }


        String[] nextLine;
        while ((nextLine = csvReader.readNext()) != null) {

            Match match = new Match(nextLine);
            match.setId(System.currentTimeMillis());

            parsedMatches.add(match);
        }

        return parsedMatches;
    }

    private void deleteExistingMatches() {
        // delete all matches from datastore
        List<Match> matches = OfyService.ofy().load().type(Match.class).list();
        log.info("delete " + matches.size() + " matches from datastore");
        OfyService.ofy().delete().entities(matches).now();
    }
}
