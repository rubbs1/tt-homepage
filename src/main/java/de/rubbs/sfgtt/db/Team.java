package de.rubbs.sfgtt.db;

import lombok.Data;

/**
 * Created by ruben on 28.12.15.
 */

@Data
public class Team {
    private String name;
    private int number;
    private int points;

    public Team(){}

    public Team(String name, int number, int points){
        this.name = name;
        this.number = number;
        this.points = points;
    }

    public Team(String[] csvLine, boolean isHome){
        if(isHome) {
            this.name = csvLine[17];
            this.number = Integer.parseInt(csvLine[19]);
            this.points = Integer.parseInt(csvLine[25]);
        }
        else{
            this.name = csvLine[23];
            this.number = Integer.parseInt(csvLine[25]);
            this.points = Integer.parseInt(csvLine[27]);
        }
    }
}
